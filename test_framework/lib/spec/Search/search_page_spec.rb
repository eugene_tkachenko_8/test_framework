require_relative '../../spec/spec_helper'
require_relative '../../services/episerver_edit_mode'
require_relative '../../services/view_mode_page'


describe "Search Page" do

  before :all do
    visit EpiserverEditMode
    on(EpiserverEditMode).login_to_episerver()
    on(EpiserverEditMode).expand_and_pin_content_tree
    on(EpiserverEditMode).create_content_page("Search", FigNewton.search_page_title)
    on(EpiserverEditMode).press_create_button
    on(EpiserverEditMode).navigate_on_all_properties_mode
    on(EpiserverEditMode).set_no_result_text(FigNewton.search_no_result_text)
    on(EpiserverEditMode).publish_current_page
    on(EpiserverEditMode).set_the_value_in_global_variable("url_of_search_page_in_edit_mode", @current_page.url_of_page_in_edit_mode)
  end

  after :all do
    on(EpiserverEditMode).navigate_on_page_modes(@current_page.get_the_value_in_global_variable("url_of_page_in_edit_mode"))
    on(EpiserverEditMode).delete_created_content_page(FigNewton.article_page_title)
  end

  context "When Search Page created and populated", regression: true, acceptance: true do
    context "When on View mode and query for unexisting page entered" do
      let!(:no_result_text) { FigNewton.search_no_result_text }
      before :all do
        on(EpiserverEditMode).navigate_on_view_mode_of_current_page_as_regular_user
        on(ViewModePage).search_through_search_page(FigNewton.search_of_unexisting_page)
      end
      it "No Result text should present" do
        expect(on(ViewModePage).search_result_value).to include no_result_text
      end
    end

    context "When search thru quick-search performed" do
      before :all do
        on(ViewModePage).open_and_search_through_quicksearch(FigNewton.search_of_unexisting_page)
      end
      it "404 Error page should be shown" do
        expect(on(ViewModePage).check_http_status).to include '404'
      end
    end

    context "When on View mode of updated Search page" do
      let!(:search_page_size) { FigNewton.search_page_size }
      let!(:search_page_title) { FigNewton.search_page_title }
      before :all do
        visit EpiserverEditMode
        on(EpiserverEditMode).login_to_episerver()
        on(EpiserverEditMode).specify_search_page_in_site_settings(FigNewton.search_page_title)
        on(EpiserverEditMode).publish_the_changes
        on(EpiserverEditMode).navigate_on_page_modes(@current_page.get_the_value_in_global_variable("url_of_search_page_in_edit_mode"))
        on(EpiserverEditMode).set_search_page_size(FigNewton.search_page_size)
        on(EpiserverEditMode).publish_the_changes
      end
      context "When search thru quick-search performed" do
        before :all do
          on(EpiserverEditMode).navigate_on_view_mode_of_current_page_as_regular_user
          on(ViewModePage).open_and_search_through_quicksearch(FigNewton.search_of_unexisting_page)
        end
        it "Search Page should be displayed" do
          expect(on(ViewModePage).title_of_current_page).to include (search_page_title)
        end
        # it "Paging should be present" do
        #   expect(on(ViewModePage).paging_is_present?).to be_truthy
        # end
        # it "Navigation thru pages should be available" do
        #   expect(on(ViewModePage).navigate_through_pagination_is_available).should be_truthy
        # end
      end
    end
  end
end