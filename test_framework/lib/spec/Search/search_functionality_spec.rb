require_relative '../../spec/spec_helper'
require_relative '../../services/episerver_edit_mode'
require_relative '../../services/view_mode_page'


describe "Search functionality" do

  before :all do
    visit EpiserverEditMode
    on(EpiserverEditMode).login_to_episerver()
    on(EpiserverEditMode).expand_and_pin_content_tree
    on(EpiserverEditMode).create_content_page("Article", FigNewton.page_title_for_content_page_for_search+" expired")
    on(EpiserverEditMode).default_text_field("Fill", FigNewton.page_heading_for_content_page_for_search+" expired", FigNewton.page_intro_for_content_page_for_search+" expired", FigNewton.page_body_for_content_page_for_search+" expired")
    on(EpiserverEditMode).make_page_expired
    on(EpiserverEditMode).publish_current_page
    on(EpiserverEditMode).set_the_value_in_global_variable("url_on_expired_content_page", @current_page.url_of_page_in_edit_mode)
    on(EpiserverEditMode).create_content_page("Article", FigNewton.page_title_for_content_page_for_search)
    on(EpiserverEditMode).default_text_field("Fill", FigNewton.page_heading_for_content_page_for_search, FigNewton.page_intro_for_content_page_for_search, FigNewton.page_body_for_content_page_for_search)
    on(EpiserverEditMode).publish_current_page
    on(EpiserverEditMode).set_the_value_in_global_variable("url_on_uniq_content_page_on_search", @current_page.url_of_page_in_edit_mode)
  end

  after :all do
    on(EpiserverEditMode).navigate_on_page_modes(@current_page.get_the_value_in_global_variable("url_of_page_in_edit_mode"))
    on(EpiserverEditMode).delete_created_content_page(FigNewton.article_page_title)
  end

  context "When all presettings are made", smoke: true, regression: true, acceptance: true do
    context "When on View mode" do
      let!(:title_of_expired_page) { FigNewton.page_title_for_content_page_for_search+" expired" }
      let!(:title_of_content_page) { FigNewton.page_title_for_content_page_for_search }
      before :all do
        on(EpiserverEditMode).navigate_on_view_mode_of_current_page_as_regular_user
        on(ViewModePage).open_and_search_through_quicksearch("lorem")
      end
      it "Just created content page should be present in result list" do
        on(ViewModePage).list_items.count.times do |i|
          expect(on(ViewModePage).list_item_heading(i)).to include title_of_content_page
        end
      end
      it "Expired content page should be absent in result list" do
        on(ViewModePage).list_items.count.times do |i|
          expect(on(ViewModePage).list_item_heading(i)).to_not include title_of_expired_page
        end
      end
    end

    context "When content page deleted" do
      before :all do
        on(EpiserverEditMode).navigate_on_page_modes(@current_page.get_the_value_in_global_variable("url_on_uniq_content_page_on_search"))
        on(EpiserverEditMode).login_to_episerver()
        on(EpiserverEditMode).delete_created_content_page(FigNewton.page_title_for_content_page_for_search)
      end
      context "And on View mode" do
        let!(:title_of_deleted_content_page) { FigNewton.page_title_for_content_page_for_search }
        before :all do
          on(EpiserverEditMode).navigate_on_view_mode_of_current_page_as_regular_user
        end
        it "Deleted content page should be absent in result list" do
          on(ViewModePage).list_items.count.times do |i|
            expect(on(ViewModePage).list_item_heading(i)).to_not include title_of_deleted_content_page
          end
        end
      end
    end
  end
end