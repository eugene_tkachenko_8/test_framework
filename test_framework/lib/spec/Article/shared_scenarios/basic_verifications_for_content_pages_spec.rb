require_relative '../../spec_helper'
require_relative '../../../services/episerver_edit_mode'
require_relative '../../../services/view_mode_page'

shared_examples "content pages common verifications" do |page_type,page_title,page_heading,page_intro,page_body,global_variable_of_page_in_edit_mode|

  before :all do
    visit EpiserverEditMode
    on(EpiserverEditMode).login_to_episerver()
    on(EpiserverEditMode).expand_and_pin_content_tree
    on(EpiserverEditMode).create_content_page(page_type, page_title)
    on(EpiserverEditMode).default_text_field("Fill", page_heading, page_intro, page_body)
    on(EpiserverEditMode).set_content_list_image
    on(EpiserverEditMode).create_new_block("Media Content", "Image with caption title", "Image with caption")
    on(EpiserverEditMode).fill_and_publish_image_with_caption_block("Caption", "Description")
    on(EpiserverEditMode).set_share_items([:facebook, :twitter])
    on(EpiserverEditMode).set_tag("Existing")
    on(EpiserverEditMode).publish_current_page
    on(EpiserverEditMode).set_the_value_in_global_variable(global_variable_of_page_in_edit_mode, @current_page.url_of_page_in_edit_mode)
  end

  shared_examples "a content page with properties" do |heading_text, intro_text, body_text, share_items|
    it "heading/intro/body should have correct values" do
      expect(on(ViewModePage).defualt_text_properties_has_correct_values).to match_array([heading_text, intro_text, body_text])
    end
    it "shared_scenarios tab should have correct values" do
      expect(on(ViewModePage).share_tab_icons).to match_array(share_items)
    end
  end

  context "When the page is created and populated", smoke: true, regression: true, acceptance: true do
    #due to we currently on Edit-mode so Tags&List Image verification(Edit-mode specific) performed here
    context "When On-page editing mode", regression: true, acceptance: true do
      let!(:test_data) { {:tag_title => FigNewton.tag_title_existing_full} }
      it "Tag panel should contain correct tag" do
        expect(on(EpiserverEditMode).tag_panel_contains_tag_with_title).to include test_data[:tag_title]
      end
    end

    context "When on View mode of #{page_type} as regular user" do
      test_data = {:heading => page_heading, :intro => page_intro, :body => page_body, :share_items => ["LinkedIn", "Google+"]}
      before :all do
        on(EpiserverEditMode).navigate_on_view_mode_of_current_page_as_regular_user
      end
      it_behaves_like "a content page with properties", test_data[:heading], test_data[:intro], test_data[:body], test_data[:share_items]
      it "Main image should be present" do
        expect(on(ViewModePage).media_content_area.img.exists?).to be_truthy
      end
    end


    context "When on updated page"do
      test_data = {:heading => page_heading+" Updated", :intro => page_intro+" updated", :body => page_body+" updated", :share_items => ["Facebook", "Tweet", "LinkedIn", "Google+"]}
      before :all do
        on(EpiserverEditMode).navigate_on_page_modes(@current_page.get_the_value_in_global_variable(global_variable_of_page_in_edit_mode))
        on(EpiserverEditMode).login_to_episerver()
        on(EpiserverEditMode).default_text_field("Update", page_heading+" updated", page_intro+" updated", page_body+" updated")
        on(EpiserverEditMode).set_share_items([:facebook, :twitter])
        on(EpiserverEditMode).delete_block_from_content_area("Media Content")
        on(EpiserverEditMode).publish_the_changes
        on(EpiserverEditMode).navigate_on_view_mode_of_current_page_as_regular_user
      end
      context "Heading/intro/body and shared_scenarios tab has correct values", smoke: true, regression: true, aceeptance: true do
        it_behaves_like "a content page with properties", test_data[:heading], test_data[:intro], test_data[:body], test_data[:share_items]
        it "Main image should be absent" do
          expect(on(ViewModePage).media_content_area.nil?).to be_falsey
        end
      end
    end
  end
end