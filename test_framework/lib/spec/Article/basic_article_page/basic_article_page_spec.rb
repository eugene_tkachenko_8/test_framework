require_relative '../../spec_helper'
require_relative '../../../services/episerver_edit_mode'
require_relative '../../../services/view_mode_page'
require_relative '../shared_scenarios/basic_verifications_for_content_pages_spec'

describe "Basic article Page" do

  after :all do
    on(EpiserverEditMode).navigate_on_page_modes(@current_page.get_the_value_in_global_variable("url_of_page_in_edit_mode"))
    on(EpiserverEditMode).delete_created_content_page(FigNewton.article_page_title)
  end

  it_behaves_like "content pages common verifications", "Article",FigNewton.article_page_title,FigNewton.article_page_heading, FigNewton.article_page_intro, FigNewton.article_page_body, "url_of_page_in_edit_mode"

end