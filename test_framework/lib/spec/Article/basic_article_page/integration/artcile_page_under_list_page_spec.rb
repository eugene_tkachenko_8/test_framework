require_relative '../../../spec_helper'
require_relative '../../../../services/episerver_edit_mode'
require_relative '../../../../services/view_mode_page'

describe "Article page under List Page" do

  before :all do
    visit EpiserverEditMode
    on(EpiserverEditMode).login_to_episerver()
    on(EpiserverEditMode).expand_and_pin_content_tree
    on(EpiserverEditMode).create_content_page("Article", FigNewton.article_page_title)
    on(EpiserverEditMode).default_text_field("Fill", FigNewton.article_page_heading, FigNewton.article_page_intro, FigNewton.article_page_body)
    on(EpiserverEditMode).set_content_list_image
    on(EpiserverEditMode).create_new_block("Media Content", "Image with caption title", "Image with caption")
    on(EpiserverEditMode).fill_and_publish_image_with_caption_block("Caption", "Description")
    on(EpiserverEditMode).publish_current_page
    on(EpiserverEditMode).copy_current_aritcle_page(FigNewton.article_page_title)
    on(EpiserverEditMode).create_content_page("List", FigNewton.list_page_title)
    on(EpiserverEditMode).publish_current_page
    on(EpiserverEditMode).set_the_value_in_global_variable("url_of_list_page_in_view_mode", @current_page.url_of_page_in_view_mode)
    on(EpiserverEditMode).paste_article_page_under_list_page(FigNewton.list_page_title)
    on(EpiserverEditMode).set_the_value_in_global_variable("url_of_page_in_edit_mode_under_list", @current_page.url_of_page_in_edit_mode)
  end

  after :all do
    on(EpiserverEditMode).navigate_on_edit_mode_of_page(@current_page.get_the_value_in_global_variable("url_of_page_in_edit_mode"))
    on(EpiserverEditMode).delete_created_content_page(FigNewton.article_page_title)
    on(EpiserverEditMode).navigate_on_page_modes(@current_page.get_the_value_in_global_variable("url_of_page_in_edit_mode_under_list"))
    on(EpiserverEditMode).delete_created_content_page(FigNewton.list_page_title)
  end

  shared_examples "list page with list item for article page" do |heading_text, intro_text|
    it "List item for article page has correct title and intro" do
      expect(on(ViewModePage).list_items_text_properties_has_correct_values(1)).to match_array([heading_text, intro_text])
    end
    it "List image from article page displayed within list item" do
      expect(on(ViewModePage).list_items_has_list_image(0)).to be_truthy
    end
  end


  context "When the Article page is created and populated ", regression: true, acceptance: true, integration: true do

    context "When on View mode of List page as regular user" do
      test_data = {:heading => FigNewton.article_page_heading, :intro => FigNewton.article_page_intro}
      before :all do
        on(EpiserverEditMode).clear_the_browser_cookies
        on(EpiserverEditMode).navigate_on_page_modes(@current_page.get_the_value_in_global_variable("url_of_list_page_in_view_mode"))
      end
       it_behaves_like "list page with list item for article page", test_data[:heading], test_data[:intro]
    end

    context "When on updated page", smoke: true, regression: true, aceeptance: true do
      test_data = {:heading => FigNewton.updated_article_page_heading, :intro => FigNewton.updated_article_page_intro}
      before :all do
        on(EpiserverEditMode).navigate_on_page_modes(@current_page.get_the_value_in_global_variable("url_of_page_in_edit_mode_under_list"))
        on(EpiserverEditMode).login_to_episerver()
        on(EpiserverEditMode).default_text_field("Update", FigNewton.updated_article_page_heading, FigNewton.updated_article_page_intro, FigNewton.updated_article_page_body)
        on(EpiserverEditMode).clear_content_list_image
        on(EpiserverEditMode).publish_the_changes
        on(EpiserverEditMode).navigate_on_page_modes(@current_page.get_the_value_in_global_variable("url_of_list_page_in_view_mode"))
      end
      it_behaves_like "list page with list item for article page", test_data[:heading], test_data[:intro]
    end
  end
end