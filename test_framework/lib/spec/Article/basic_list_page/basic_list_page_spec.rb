require_relative '../../spec_helper'
require_relative '../../../services/episerver_edit_mode'
require_relative '../../../services/view_mode_page'
require_relative '../shared_scenarios/basic_verifications_for_content_pages_spec'

describe "Basic List page" do

  it_behaves_like "content pages common verifications", "List", FigNewton.list_page_title, FigNewton.list_page_heading, FigNewton.list_page_intro, FigNewton.list_page_body, "url_of_list_page_in_edit_mode"

  after :all do
    on(EpiserverEditMode).navigate_on_edit_mode_of_page_from_view_mode(@current_page.get_the_value_in_global_variable("url_of_list_page_in_edit_mode"))
    on(EpiserverEditMode).delete_created_content_page(FigNewton.list_page_title)
  end

  shared_examples "a list page with list items" do |heading_text, intro_text, published_date_presence, list_image_presence|
    it "heading/intro/link and publish date should have correct values", smoke: true do
      on(ViewModePage).list_items.count.times do |i|
        expect(on(ViewModePage).list_items_text_properties_has_correct_values(i)).to match_array([heading_text, intro_text])
        expect(on(ViewModePage).list_items_has_published_date(i)).to be published_date_presence
        expect(on(ViewModePage).list_items_has_list_image(i)).to be list_image_presence
      end
    end
  end

  context "When the List page is created and populated", smoke: true, regression: true, acceptance: true do
    before :all do
      on(EpiserverEditMode).navigate_on_page_modes(@current_page.get_the_value_in_global_variable("url_of_list_page_in_edit_mode"))
      on(EpiserverEditMode).login_to_episerver()
      on(EpiserverEditMode).set_tag_filter(FigNewton.tag_title_existing_full)
      on(EpiserverEditMode).publish_the_changes
      on(EpiserverEditMode).set_the_value_in_global_variable("url_of_list_page_on_view_mode", @current_page.url_of_page_in_view_mode)
      on(EpiserverEditMode).create_content_pages_under_list_page(3, "Article", FigNewton.list_page_title, "Article under list page with existing tag", "Existing", true)
      on(EpiserverEditMode).create_content_pages_under_list_page(3, "List", FigNewton.list_page_title, "List under list page", "New", true)
    end
    context "When on View mode of List page" do
      test_data = {:heading => "Article Under List Page With Existing Tag", :intro => FigNewton.article_page_intro, :published_date_presence => true, :list_image_presence => true}
      before :all do
        on(EpiserverEditMode).clear_the_browser_cookies
        on(EpiserverEditMode).navigate_on_page_modes(@current_page.get_the_value_in_global_variable("url_of_list_page_on_view_mode"))
      end
      it_behaves_like "a list page with list items", test_data[:heading], test_data[:intro], test_data[:published_date_presence], test_data[:list_image_presence]
      it "Only list items for pages with corresponding tag should be present", regression: true, acceptance: true do
        on(ViewModePage).list_items.count.times do |i|
          expect(on(ViewModePage).list_items_for_pages_with_specific_tag(i)).to include "Existing Tag"
        end
      end
    end

    context "When updated List page viewed", smoke: true, regression: true, acceptance: true do
      test_data = {:heading => "List Under List Page", :intro => "Absent", :published_date_presence => false, :list_image_presence => true, :list_layout => "tiles"}
      let!(:page_size) { FigNewton.list_page_size }
      before :all do
        on(EpiserverEditMode).navigate_on_page_modes(@current_page.get_the_value_in_global_variable("url_of_list_page_in_edit_mode"))
        on(EpiserverEditMode).login_to_episerver()
        on(EpiserverEditMode).remove_tag_filter_setting
        on(EpiserverEditMode).publish_the_changes
        on(EpiserverEditMode).create_content_page("List", FigNewton.new_list_page_title)
        on(EpiserverEditMode).set_created_list_page_as_list_root(FigNewton.list_page_title)
        on(EpiserverEditMode).set_list_layout("Tiles")
        on(EpiserverEditMode).filter_by_page_type("List")
        on(EpiserverEditMode).list_item_specific_properties(true, true, 2)
        on(EpiserverEditMode).publish_current_page
        on(EpiserverEditMode).navigate_on_view_mode_of_current_page_as_regular_user
      end
      it_behaves_like "a list page with list items", test_data[:heading], test_data[:intro], test_data[:published_date_presence], test_data[:list_image_presence]
      it "List items should have corresponding list layout", regression: true, acceptance: true do
        expect(on(ViewModePage).list_items_layout).to include test_data[:list_layout]
      end
      it "Count of list items should corresponds to page size" do
        expect(on(ViewModePage).list_items_on_page(page_size)).to be_truthy
      end
      it "Paging should be present" do
        expect(on(ViewModePage).paging_is_present?).to be_truthy
      end
      it "Navigation thru pages should be available" do
        expect(on(ViewModePage).navigate_through_pagination_is_available).should be_truthy
      end
      it "Only list items for pages with corresponding type should be present", regression: true, acceptance: true do
        on(ViewModePage).list_items.count.times do |i|
          expect(on(ViewModePage).list_items_for_corresponding_page_type("List", i)).to be_truthy
        end
      end
    end
  end
end