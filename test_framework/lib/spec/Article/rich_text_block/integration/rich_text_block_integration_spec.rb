require_relative '../../../spec_helper'
require_relative '../../../../services/episerver_edit_mode'
require_relative '../../../../services/view_mode_page'

shared_examples "rich text block on content page" do |page_type,page_title,global_variable|

  before :all do
    visit EpiserverEditMode
    on(EpiserverEditMode).login_to_episerver()
    on(EpiserverEditMode).expand_and_pin_content_tree
    on(EpiserverEditMode).create_content_page(page_type, page_title)
    on(EpiserverEditMode).create_new_block("Advanced Content", "Rich Text Block in advanced content", "Rich Text Block")
    on(EpiserverEditMode).fill_and_publish_richtext_block("Rich text body text")
    on(EpiserverEditMode).create_new_block("Top Sidebar Content", "Rich Text Block in top sidebar content", "Rich Text Block")
    on(EpiserverEditMode).fill_and_publish_richtext_block("Rich text body text")
    on(EpiserverEditMode).create_new_block("Bottom Sidebar Content", "Rich Text Block in bottom sidebar content", "Rich Text Block")
    on(EpiserverEditMode).fill_and_publish_richtext_block("Rich text body text")
    on(EpiserverEditMode).publish_current_page
    on(EpiserverEditMode).set_the_value_in_global_variable(global_variable, @current_page.url_of_page_in_edit_mode)
    on(EpiserverEditMode).navigate_on_view_mode_of_current_page_as_regular_user
  end

  after :all do
    on(EpiserverEditMode).navigate_on_edit_mode_of_page_from_view_mode(@current_page.get_the_value_in_global_variable(global_variable))
    on(EpiserverEditMode).delete_created_content_page(page_title)
  end

  context "When on View mode of #{page_type} Page" do

    context "And the blocks are added to #{page_type} page", regression: true, acceptance: true, integration: true do
      it "Rich text block should be present in advanced content" do
        expect(on(ViewModePage).advanced_content_presence).to be_truthy
      end
      it "Rich text block should be present in top sidebar content" do
        expect(on(ViewModePage).top_sidebar_content_presence).to be_truthy
      end
      it "Rich text block should be present in bottom sidebar content" do
        sleep 2
        expect(on(ViewModePage).bottom_sidebar_content_presence).to be_truthy
      end
    end

    context "When the blocks are deleted from corresponding areas of #{page_type} Page", regression: true, acceptance: true, integration: true do
      before :all do
        on(EpiserverEditMode).navigate_on_page_modes(@current_page.get_the_value_in_global_variable(global_variable))
        on(EpiserverEditMode).login_to_episerver()
        on(EpiserverEditMode).delete_block_from_content_area("Advanced Content")
        on(EpiserverEditMode).delete_block_from_content_area("Top Sidebar Content")
        on(EpiserverEditMode).delete_block_from_content_area("Bottom Sidebar Content")
        on(EpiserverEditMode).publish_the_changes
        on(EpiserverEditMode).navigate_on_view_mode_of_current_page_as_regular_user
      end
      it "Rich text block should be absent in advanced content" do
        expect(on(ViewModePage).advanced_content_presence).to be_truthy
      end
      it "Rich text block should be absent in top sidebar content" do
        expect(on(ViewModePage).top_sidebar_content_presence).to be_falsey
      end
      it "Rich text block should be absent in bottom sidebar content" do
        expect(on(ViewModePage).bottom_sidebar_content_presence).to be_falsey
      end
    end
  end


end

