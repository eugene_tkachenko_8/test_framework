require_relative '../../../spec_helper'
require_relative '../../../../services/episerver_edit_mode'
require_relative '../../../../services/view_mode_page'

describe "Rich Text block on List Page" do

  it_behaves_like "rich text block on content page", "List", FigNewton.list_page_title_for_rich_text_block, "url_of_list_page_with_rich_text_in_edit_mode"

end