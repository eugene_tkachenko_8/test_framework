require_relative '../../../spec_helper'
require_relative '../../../../services/episerver_edit_mode'
require_relative '../../../../services/view_mode_page'
require_relative 'rich_text_block_integration_spec'

describe "Rich Text block on Article Page" do

  it_behaves_like "rich text block on content page", "Article", FigNewton.basic_article_page_title_with_rich_text_block, "url_of_article_page_with_rich_text_in_edit_mode"

end