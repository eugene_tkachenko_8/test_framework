require_relative '../../spec_helper'
require_relative '../../../services/episerver_edit_mode'
require_relative '../../../services/view_mode_page'

describe "Rich Text block" do

  before :all do
    visit EpiserverEditMode
    on(EpiserverEditMode).login_to_episerver()
    on(EpiserverEditMode).expand_and_pin_content_tree
    on(EpiserverEditMode).create_content_page("Article", FigNewton.article_page_title_for_rich_text_block)
    on(EpiserverEditMode).create_new_block("Advanced Content", "Rich Text Block in advanced content", "Rich Text Block")
    on(EpiserverEditMode).fill_and_publish_richtext_block("Rich text body text")
    on(EpiserverEditMode).create_new_block("Top Sidebar Content", "Rich Text Block in top sidebar content", "Rich Text Block")
    on(EpiserverEditMode).fill_and_publish_richtext_block("Rich text body text")
    on(EpiserverEditMode).create_new_block("Bottom Sidebar Content", "Rich Text Block in bottom sidebar content", "Rich Text Block")
    on(EpiserverEditMode).fill_and_publish_richtext_block("Rich text body text")
    on(EpiserverEditMode).publish_current_page
    on(EpiserverEditMode).set_the_value_in_global_variable("url_of_article_page_with_rich_text_in_edit_mode_integration", @current_page.url_of_page_in_edit_mode)
    on(EpiserverEditMode).navigate_on_view_mode_of_current_page_as_regular_user
  end

  after :all do
    on(EpiserverEditMode).navigate_on_edit_mode_of_page_from_view_mode(@current_page.get_the_value_in_global_variable("url_of_article_page_with_rich_text_in_edit_mode_integration"))
    on(EpiserverEditMode).delete_created_content_page(FigNewton.article_page_title_for_rich_text_block)
  end

  context "When the block is added to page", regression: true, acceptance: true do
    test_data = {:rich_text_block_value => "Rich text body text"}
    it "Rich text block should be present in advanced content and have correct value" do
      expect(on(ViewModePage).advanced_content_area_value).to include test_data[:rich_text_block_value]
    end
    it "Rich text block should be present in top sidebar content and have correct value" do
      expect(on(ViewModePage).top_sidebar_content_area_value).to include test_data[:rich_text_block_value]
    end
    it "Rich text block should be present in bottom sidebar content and have correct value" do
      expect(on(ViewModePage).bottom_sidebar_content_area_value).to include test_data[:rich_text_block_value]
    end
  end
end