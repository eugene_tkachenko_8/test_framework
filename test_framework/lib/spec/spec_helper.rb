#encoding: UTF-8

require 'fig_newton'
require 'rspec'
require 'page-object'
require 'watir-webdriver'
require 'selenium-webdriver'
require 'yaml' #Built in, no gem required
require 'psych' #Needed for to normal work with yaml
require 'net/http' #Needed to get http responses code
require 'uri'

# Need to make alias for FigNewton module to show the assignment of module
# E.x.: DATAFILE = Fignewton (Fignewton just a module)
# Then we can use 'DATAFILE.article_page_heading' to get the value from configured FigNewton file

#
FigNewton.yml_directory = 'test_data'
FigNewton.load('test_data.yml')

RSpec.configure do |config|
  config.include PageObject::PageFactory

  config.before :all do
    @browser = Watir::Browser.new :firefox
    @browser.window.maximize
  end

  config.after :all do
    @browser.close
  end
end

