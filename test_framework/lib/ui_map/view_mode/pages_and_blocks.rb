#encoding: UTF-8
require_relative '../../spec/spec_helper'

module PagesandBlocks

  include PageObject


  page_url 'specify_url'

  ############################ Page elements ###############################
  h1(:content_page_heading, :class => 'main-title')
  div(:content_page_intro, :class => 'preamble')
  element(:content_area_of_the_page, :article, :class => 'col-lg-6 content')
  div(:content_page_body, :class => 'richtext')

  def heading_value #The page will always have a heading
    content_page_heading_element.text.split.map(&:capitalize).join(' ')
  end

  def intro_value
    if content_page_intro_element.exists?
      content_page_intro_element.text
    else
      "Absent"
    end
  end

  def body_value
    if content_page_body_element.exists?
      content_page_body_element.text
    else
      "Absent"
    end
  end

  def media_content_area
    @browser.jq('div.media-content')
  end

  def advanced_content_area
    @browser.divs(:xpath => "//div[@class='block-wrapper']")[0]
  end

  def top_sidebar_content_area
    @browser.divs(:xpath => "//aside[contains(@class,'sidebar')]/div")[0]

  end

  def bottom_sidebar_content_area
    @browser.divs(:xpath => "//aside[contains(@class,'sidebar')]/div")[1]
  end

  def list_image_within_list_item
    @browser.jq('div.list-item img')
  end

  def advanced_content_area_value
    @browser.divs(:xpath => "//div[@class='richtext']/p")[0].text
  end

  def top_sidebar_content_area_value
    @browser.divs(:xpath => "//div[@class='richtext']/p")[1].text

  end

  def bottom_sidebar_content_area_value
    @browser.divs(:xpath => "//div[@class='richtext']/p")[2].text
  end

  def list_items
    @browser.ul(:class => /list/).lis
  end

  def list_item_heading_value(list_item_num)
    list_items[list_item_num].h3.text.split.map(&:capitalize).join(' ')
  end

  def list_item_heading(list_item_num)
    list_items[list_item_num].h3
  end

  def list_item_intro_value(list_item_num)
    if list_items[list_item_num].p.exists?
      list_items[list_item_num].p.text
    else
      "Absent"
    end
  end

  def publish_date_presence(list_item_num)
    list_items[list_item_num].div(:class => "publish").exists?
  end

  def list_image_presence(list_item_num)
    list_items[list_item_num].img.present?
  end

  def list_area_layout
    @browser.div(:xpath => '//div[contains(@class,"list-wrapper")]').ul.attribute_value("class")
  end

  def paging_block
    @browser.ul(:class => 'pagination')
  end

  def paging_nums(i)
    @browser.ul(:class => 'pagination').lis[i].link
  end
end
