require_relative '../../spec/spec_helper'
require 'watir-jquery'

module AssetPanel

  include PageObject
  include WatirJquery

  def toggle_asset_panel
    @browser.jq('span[title=Toggle assets pane]').click
  end

  def pin_asset_panel
    @browser.jq('span[title="Pin"]').click
  end

  def context_menu_on_global_assets
    @browser.jq('div[title=Global Assets] span[title=Options]:first').to_subtype
  end

  def new_block_option_in_context_menu
    @browser.jq('td:contains("New Block"):last').to_subtype
  end

  def rich_text_block_in_tree
    @browser.div(:xpath => '//div[contains(@title,"Rich text block title")]')
  end

end