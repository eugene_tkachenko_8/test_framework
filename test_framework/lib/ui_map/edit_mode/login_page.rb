require_relative '../../spec/spec_helper'
require 'watir-jquery'

module LoginPage

  include PageObject
  include WatirJquery

  def set_login(login_value)
    @browser.jq('input[id$="UserName"]').to_subtype.set(login_value)
  end

  def set_password(password_value)
    @browser.jq('input[id$="Password"]').to_subtype.set(password_value)
  end

  def click_on_login_button
    @browser.jq('input[id$="Button1"]').click
  end

end