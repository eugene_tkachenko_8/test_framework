require_relative '../../spec/spec_helper'
require 'watir-jquery'

module Properties

  include PageObject
  include WatirJquery

  unordered_list(:pages_list, :class => 'epi-advancedListing')
  span(:publish_menu, :text => 'Publish?')
  span(:publish_button, :text => 'Publish')
  span(:options_menu, :text => 'Options')
  span(:publish_update_button, :text => 'Publish Changes')
  span(:in_line_editing_mode, :title => 'All Properties')
  span(:on_page_editing_mode, :title => 'On-Page Editing')
  link(:page_href, :text => 'View on website')
  divs(:heading_area, :class => 'epi-overlay-item-container')
  divs(:tags, :id => /uniqName_101_.*/)
  span(:open_mini_content_tree, :text => "...")
  text_field(:tag_field, :id => /s2id_autogen.*/)
  link(:global_assets, :title => "Global Assets")
  span(:ok_button_in_current_context, :text => "OK")
  span(:done_button_in_current_context, :text => "Done")
  span(:site_settings_tab, :text => "Site Settings")
  span(:create_button, :text => 'Create')
  span(:back_link, :text => 'Back')
  cells(:menu_items, :text => 'Move to Trash')
  in_iframe(:id => /uniqName_.*_0/) do |frame| ## for tags
    div(:tags_list_on_page_editing, :xpath => "//div[contains(@data-epi-property-name,'Tags')]", :frame => frame)
  end
  in_iframe(:id => /uniqName_.*_0/) do |frame| ## for heading text
    element(:content_default_property, :article, :class => 'col-lg-6 content', :frame => frame)
  end
  in_iframe(:id => /uniqName_.*_0_editorFrame_ifr/) do |frame| ## for intro text
    element(:intro_area, :body, :id => 'tinymce', :frame => frame)
  end
  in_iframe(:id => /uniqName_.*_1_editorFrame_ifr/) do |frame| ## for body text
    element(:body_area, :body, :id => 'tinymce', :frame => frame)
  end

  def name_of_page_field
    @browser.text_fields(:id => /dijit_form_ValidationTextBox_.*/)[3]
  end

  def iframe_for_page_properties
    @browser.iframes(:id => /uniqName_.*_.*_editorFrame_ifr/)
  end

  def heading_property
    @browser.iframe(:id => /uniqName_.*_0/).h1(:xpath => '//h1[contains(@class,"main-title")]')
  end

  def intro_property
    iframe_for_page_properties[1].element(:xpath => '//body[contains(@id,"tinymce")]')
  end

  def body_property
    iframe_for_page_properties[2].element(:xpath => '//body[contains(@id,"tinymce")]')
  end

  def create_in_advanced_content_area
    @browser.links(:text => 'create a new block')[1]
  end

  def create_in_media_content_area
    @browser.links(:text => 'create a new block')[0]
  end

  def create_in_top_sidebar_content
    @browser.links(:text => 'create a new block')[2]
  end

  def create_in_bottom_sidebar_content
    @browser.links(:text => 'create a new block')[3]
  end

  def image_withing_media_tree
    @browser.span(:xpath => '//span[contains(text(),".png")]')
  end

  def open_tags_box
    begin
      sleep 2
      tags_elements[1].click
    rescue
      retry
    end
  end

  def open_share_tab_settings
    @browser.div(:xpath => '//span[contains(text(),"Sharing")]/following-sibling::div').click
  end

  def define_existing_tag(tag_title)
    self.tag_field = tag_title
    sleep 2
    @browser.send_keys :arrow_down
    sleep 2
    @browser.send_keys :enter
  end

  def define_new_tag(tag_title)
    self.tag_field = tag_title
    sleep 2
    @browser.send_keys :enter
  end

  def set_the_value_to_the_heading(heading_text)
    sleep 3
    heading_area_elements[4].click
    sleep 2
    heading_property.send_keys heading_text
    sleep 1
    @browser.send_keys :enter
    sleep 2
  end

  def set_the_value_to_the_intro(intro_text)
    ## For to not refresh the page (first time it can fails so  first - hover and activate element, second - click)
    begin
      sleep 2
      @browser.divs(:class => 'epi-overlay-item-container')[5].click
    rescue
      retry
    end
    sleep 2
    intro_property.send_keys intro_text
    sleep 2
  end

  def set_the_value_to_the_body(body_text)
    ## For to not refresh the page (first time it can fails so  first - hover and activate element, second - click)
    begin
      @browser.divs(:class => 'epi-overlay-item-container')[6].click
      sleep 5
    rescue
      retry
    end
    sleep 4
    body_property.send_keys body_text
    sleep 2
  end

  def clear_the_value_from_the_heading
    heading_area_elements[4].click
    sleep 2
    content_default_property_element.h1_element(:class => 'main-title').send_keys [:control, "a"]
    content_default_property_element.h1_element(:class => 'main-title').send_keys :delete
    imitate_enter_key_action
    sleep 2
  end

  def clear_the_value_from_the_intro
    ## For to not refresh the page (first time it can fails so  first - hover and activate element, second - click)
    begin
      @browser.divs(:class => 'epi-overlay-item-container')[5].click
    rescue
      retry
    end
    sleep 4
    intro_area_element.send_keys [:control, "a"]
    intro_area_element.send_keys :delete
    sleep 2
  end

  def clear_the_value_from_the_body
    ## For to not refresh the page (first time it can fails so  first - hover and activate element, second - click)
    begin
      @browser.divs(:class => 'epi-overlay-item-container')[6].click
    rescue
      retry
    end
    sleep 5
    body_area_element.send_keys [:control, "a"]
    body_area_element.send_keys :delete
    sleep 2
  end

  def update_the_value_in_heading(heading_text)
    heading_area_elements[4].click
    content_default_property_element.h1_element(:class => 'main-title').send_keys [:control, "a"]
    content_default_property_element.h1_element(:class => 'main-title').send_keys :delete
    sleep 2
    content_default_property_element.h1_element(:class => 'main-title').send_keys "#{heading_text}"
    @browser.send_keys :enter
    sleep 2
  end

  def update_the_value_in_intro(intro_text)
    ## For to not refresh the page (first time it can fails so  first - hover and activate element, second - click)
    begin
      @browser.divs(:class => 'epi-overlay-item-container')[5].click
    rescue
      retry
    end
    sleep 5
    intro_area_element.send_keys [:control, "a"]
    intro_area_element.send_keys :delete
    sleep 2
    intro_area_element.send_keys "#{intro_text}"
    sleep 2
  end

  def update_the_value_in_body(body_text)
    ## For to not refresh the page (first time it can fails so  first - hover and activate element, second - click)
    begin
      @browser.divs(:class => 'epi-overlay-item-container')[6].click
    rescue
      retry
    end
    sleep 5
    body_area_element.send_keys [:control, "a"]
    body_area_element.send_keys :delete
    sleep 2
    body_area_element.send_keys "#{body_text}"
  end

  def navigate_on_all_properties_mode
    in_line_editing_mode_element.when_present.click
  end

  def navigate_on_on_page_editing_mode
    on_page_editing_mode_element.click
  end

  def open_mini_content_tree
    open_mini_content_tree_element.click
  end

  def expand_global_assets
    global_assets_element.span_elements[1].click
  end

  def choose_image_and_submit(image_title)
    link_element(:title => image_title).click
    sleep 2
    ok_button_in_current_context_element.click
  end

  def choose_image_in_media_tree
    open_mini_content_tree_element.click
    sleep 3
    image_withing_media_tree.click
    sleep 2
    ok_button_in_current_context_element.click
  end

  def expand_and_choose_image_in_media_tree
    open_mini_content_tree_element.click
    sleep 3
    expand_global_assets
    sleep 3
    image_withing_media_tree.click
    sleep 2
    ok_button_in_current_context_element.click
  end

  def publish_current_page
    Watir::Wait.until { publish_menu_element.visible? }
    publish_menu_element.click
    sleep 2
    publish_button_element.click
  end

  def create_block
    create_button_element.click
  end

  def publish_the_changes
    Watir::Wait.until { publish_menu_element.visible? }
    publish_menu_element.click
    sleep 2
    publish_update_button_element.click
  end

  def share_items
    share_items = @browser.elements(:xpath => '//div[contains(@title,"Content")]/ul/li//input')
    share_icons = {:facebook => share_items[0], :twitter => share_items[1], :linkedin => share_items[2], :google => share_items[3]}
    return share_icons
  end

  def remove_option_for_media_block_item
    @browser.elements(:xpath => "//td[contains(text(),'Remove')]")[5].click
  end

  def media_block_menu
    @browser.span(:xpath => "//span[contains(concat(' ',normalize-space(@class),' '),'epi-overlay-content')]")
  end

  def advance_block_menu
    @browser.span(:xpath => "//span[contains(concat(' ',normalize-space(@class),' '),'epi-overlay-content')]")
  end

  def top_sidebar_content_menu
    @browser.spans(:xpath => "//span[contains(concat(' ',normalize-space(@class),' '),'epi-overlay-content')]")[1]
  end

  def bottom_sidebar_content_menu
    @browser.spans(:xpath => "//span[contains(concat(' ',normalize-space(@class),' '),'epi-overlay-content')]")[1]
  end

  def remove_option_for_block_item
    @browser.elements(:xpath => "//td[contains(text(),'Remove')]")[7].click
  end

  def clear_button
    @browser.div(:xpath => "//div[@class='epi-clearButton']")
  end

  def get_tag_title_from_tag_bar_on_page_editing_mode
    tags_list_on_page_editing_element.unordered_list_element.list_item_element.text
  end

  def list_settings
    @browser.divs(:class => 'epi-overlay-item-container')[8]
  end

  def tags_filter
    @browser.div(:xpath => '//div[contains(@id,"app_TagSelector")]').text_field
  end

  def remove_tag_filter
    @browser.div(:xpath => '//div[contains(@id,"app_TagSelector")]').link(:xpath => '//a[contains(@class,"close")]')
  end

  def list_settings_block
    @browser.div(:xpath => '//div[contains(@id,"app_TagSelector")]')
  end

  def page_type_filter_dropdown
    list_settings_block.elements(:xpath => '//input[contains(@class,"dijitArrowButtonInner")]').last
  end

  def page_type_filter_option(page_type)
    @browser.tds(:text => "#{page_type}")[1]
  end

  def list_item_layout_dropdown
    list_settings_block.elements(:xpath => '//input[contains(@class,"dijitArrowButtonInner")]')[1]
  end

  def list_item_layout_option(list_layout)
    @browser.tds(:text => "#{list_layout}").first
  end

  def set_page_in_mini_content_tree(page_title)
    @browser.spans(:text => "#{page_title}").last
  end

  def publish_date_checkbox_list_setting
    list_settings_block.checkboxes(:xpath => '//input[contains(@id,"dijit_form_CheckBox")]')[1]
  end

  def intro_checkbox_list_setting
    list_settings_block.checkboxes(:xpath => '//input[contains(@id,"dijit_form_CheckBox")]')[2]
  end

  def page_size_setting
    list_settings_block.text_field(:xpath => '//input[contains(@id,"dijit_form_NumberSpinner")]')
  end

end