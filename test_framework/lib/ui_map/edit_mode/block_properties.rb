require_relative '../../spec/spec_helper'
require 'watir-jquery'

module BlockProperties

  include PageObject
  include WatirJquery

  def iframe_for_block_properties
    @browser.iframes(:id => /uniqName_.*_.*_editorFrame_ifr/)
  end

  def block_rich_text
      iframe_for_page_properties[1].element(:xpath => '//body[contains(@id,"tinymce")]')
  end

  def image_withing_media_tree
    @browser.span(:xpath=>'//span[contains(text(),".png")]')
  end

  def caption_field_within_image_block
    @browser.text_field(:name=>'caption')
  end

  def description_field_within_image_block
    @browser.text_field(:name=>'description')
  end

end