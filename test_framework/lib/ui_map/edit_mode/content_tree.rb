require_relative '../../spec/spec_helper'
require 'watir-jquery'

module ContentTree

  include PageObject
  include WatirJquery

  def set_login(login_value)
    @browser.jq('input[id$="UserName"]').to_subtype.set(login_value)
  end

  def toggle_content_tree
    @browser.jq('span[title="Toggle navigation pane"]').click
  end

  def pin_content_tree
    @browser.jq('span[title="Pin"]')[1].click
  end

  def site_root_page
    @browser.jq('div:first[title*="Site Root"]')
  end

  def navigate_on_specific_page(page_title)
    @browser.div(:title=>/#{page_title}/)
  end

  def context_menu_on_site_root
    @browser.jq('div:first[title*="Site Root"] span[class$="ContextMenu"]').to_subtype
  end

  def new_page_option_in_context_menu
    @browser.jq('td:contains("New Page"):last').to_subtype
  end

  def copy_page_option_in_context_menu
    @browser.jq('td:contains("Copy"):last').to_subtype
  end

  def paste_page_option_in_context_menu
    @browser.jq('td:contains("Paste"):last').to_subtype
  end

  def context_menu_on_specific_page(page_title)
    div_element(:xpath=> "//div[contains(@title,'#{page_title}')]").span_elements[5]
  end

end