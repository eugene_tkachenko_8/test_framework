# def callbacks(procs={})      Method which can get multiply blocks as args
#
#   for i in 1..procs.length
#     procs[i].call
#   end
# end
#
# callbacks(1 => Proc.new { puts "Starting" },
#           2 => Proc.new { puts "Finishing" })

# def wait                                                              Waiting for multiply elements
#   Watir::Wait.until {
#     @browser.text_field(:css => '.inputtext').exists? &&
#         @browser.button(:css => 'input#u_0_1').exists? &&
#         @browser.text_field(:css=> '.inputpassword').exists?
#   }
# end
# 2.times do
# target = open("article.txt", 'a')
# target.write("first\n")
# # target.write("\n")
# target.close
# end

# f = File.open("article.txt")
# a = f.readlines
# puts a[1]
#
# puts "http://osl-utv2k12dem1.oslutv.int:8080/automation3.-test-list/automation.-test-article/\n".gsub!("\n", "")

# def yield_arg_to(arg)
# ret = yield(arg)
# puts %Q{The block yielded "#{ret}" when passed "#{arg}"}
# end
# yield_arg_to(10) {|i| i * 1337 }
# yield_arg_to("Hello") {|s| s + " World" }
# yield_arg_to(45.2) do|r|
#  "The circumference is #{3.14 * 2 * r}"
# end


require 'yaml' # Built in, no gem required
require 'psych'
#
#
# section_title = "1"
#
# d = YAML::load_file('global_variables.yml') #Load
# d["Section_#{section_title}"] = "some" #Modify
# File.open('global_variables.yml', 'w') { |f| f.write d.to_yaml } #Store
# puts "#{d['Section_1']}"


# module Bmodule
#   def greet
#     puts 'hello world'
#   end
# end
#
# module Amodule
#       include Bmodule
# end
#
# class MyClass
#   include Amodule
# end
#
# some = MyClass.new # => hello world
# some.greet

# #1 Task#############################################
# puts "Enter the N of brackets"
# brackets = []
# n = gets.chomp.to_i
#
# if n % 2 == 0
#      print "(#{n.times("()")})"
# else
#   n.times do |i|
#     brackets << "()"
#     print "#{brackets[i]}"
#   end
# end
#
# #3 Task##############################################
#
#
# factorial_num = (1..100).inject { |multy, next_value| multy *= next_value } # the value from each iteration we multiply on number from next iteration which imitates
#                                                                             # 100*99=9900*98...
# # Now when we have some number like 933262154439441526816992388562667004907E+ we can split this number on numbers like 9+3+3+2+6+2 etc.
#
# puts factorial_num.to_s.split('').inject(0) { |sum, next_val| sum + next_val.to_i } # due to now we have Bignum value we should place it to some data type which
#                                                                                     # can include such long value so we can use String



# def heading_value_has_correct_value(heading_text) #The page will always have a heading
#     true ? "You".include?("#{heading_text}") : false
# end
#
# def intro_value_has_correct_value(intro_text)
#   if !intro_text.empty?
#     true ? "You".include?("#{intro_text}") : false
#   end
# end
#
# def body_value_has_correct_value(body_text)
#   if !body_text.empty?
#     true ? "You".include?("#{"#{body_text}"}") : false
#   end
# end
#
#
# def defualt_text_properties_has_correct_values(heading_text, intro_text, body_text)
#  puts true ? heading_value_has_correct_value(heading_text) && intro_value_has_correct_value(intro_text) & body_value_has_correct_value(body_text) : false
# end
#
# defualt_text_properties_has_correct_values("You","Some","You")

# def set_the_value_in_global_variable(variable,value)
#   file = YAML::load_file('test_data/global_variables.yml') #Load
#   file["#{variable}"] = value #Modify
#   File.open('global_variables.yml', 'w') { |f| f.write file.to_yaml } #Store
# end
#
# def get_the_value_in_global_variable(variable)
#   file = YAML::load_file('global_variables.yml') #Load
#   file["#{variable}"]
# end
#
# puts get_the_value_in_global_variable("Page_view_link_1")

# def heading_value #The page will always have a heading
#   "HEADING".capitalize
# end
#
# def intro_value
#    "Intro" ? true : "Absent"
# end
#
# def body_value
#   "Body" ? true : "Absent"
# end
#
# def defualt_text_properties_has_correct_values
#   default_text_properties = [heading_value,intro_value,body_value]
# end
#
# print defualt_text_properties_has_correct_values

# puts "AUTOMATION. ARTICLE HEADING".split.map(&:capitalize).join(' ')

# class Parent
#
#   def get_name
#     puts name
#   end
#
#   protected
#   def name
#     'Mommy'
#   end
# end
#
# class Child < Parent
#   def get_parent_name
#     # Implicit receiver
#     puts name
#
#     # Explicit receiver
#     puts self.name rescue puts 'NoMethodError'
#
#     # Explicit receiver
#     puts Parent.new.name rescue puts 'NoMethodError'
#   end
# end
#
#
# Child.new.get_parent_name

# new = "458, 444, 555".gsub(/\s+/, "")
# @array = new.split(",")
# @array.map(&:to_i)
# @array.delete(444)

# text = "Some"
# puts text[0,2]

def share_items
  some_array = ["facebook1","twitter1"]
  some_hash = {:facebook => "#{some_array[0]}", :twitter => "#{some_array[1]}"}
  return some_hash
end

def set_share_items(some)
  some.each do |icon|
   puts share_items[icon]
    end
end

set_share_items([:facebook])


