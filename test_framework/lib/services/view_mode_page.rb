#encoding: UTF-8
require_relative '../spec/spec_helper'
require_relative '../ui_map/view_mode/pages_and_blocks'

class ViewModePage

  include PageObject
  include PagesandBlocks


  page_url 'specify_url'

  def defualt_text_properties_has_correct_values
    default_text_properties = [heading_value, intro_value, body_value]
  end

  def list_items_text_properties_has_correct_values(i)
    text_properties = [list_item_heading_value(i), list_item_intro_value(i)]
  end

  def list_items_has_published_date(i)
    publish_date_presence(i)
  end

  def list_items_has_list_image(i)
    list_image_presence(i)
  end

  def share_tab_icons
    share_items = @browser.jq('a[class^="addthis_button"]')
    share_icons = []
    share_items.each do |title|
      share_icons << title.title
    end
    return share_icons
  end

  def media_content_presense
    media_content_area.visible?
  end

  def advanced_content_presence
    advanced_content_area.visible?
  end

  def top_sidebar_content_presence
    top_sidebar_content_area.visible?
  end

  def bottom_sidebar_content_presence
    bottom_sidebar_content_area.visible?
  end

  def list_image_within_list_item_presence
    list_image_within_list_item.visible?
  end

  def list_items_for_pages_with_specific_tag(i)
    list_item_heading_value(i)
  end

  def list_items_layout
    list_area_layout
  end

  def paging_is_present?
    paging_block.visible?
  end

  def navigate_through_pagination_is_available
    paging_nums(1).click
    sleep 2
    list_item_heading(0).visible?
  end

  def list_items_for_corresponding_page_type(page_type, list_count)
    list_item_heading_value(list_count).include? page_type
  end

  def list_items_on_page(page_size)
    list_items.count == page_size
  end

end
