#encoding: UTF-8
require 'page-object'
require 'watir-webdriver'
require 'selenium-webdriver'
require_relative '../spec/spec_helper'
require_relative '../ui_map/edit_mode/login_page'
require_relative '../ui_map/edit_mode/content_tree'
require_relative '../ui_map/edit_mode/properties'
require_relative '../ui_map/edit_mode/asset_panel'
require_relative '../ui_map/edit_mode/block_properties'


class EpiserverEditMode

  include PageObject
  include LoginPage
  include ContentTree
  include Properties
  include AssetPanel
  include BlockProperties

  page_url 'dev-cbaweb01001.osldev.creuna.int/EPiServer/Cms/'

  def login_to_episerver(login = FigNewton.login, password = FigNewton.password)
    sleep 3
    set_login(login)
    set_password(password)
    click_on_login_button
  end

  def expand_and_pin_content_tree
    toggle_content_tree
    pin_content_tree
  end

  def expand_and_pin_asset_panel
    toggle_asset_panel
    pin_asset_panel
  end

  def create_content_page(page_type, page_title)
    navigate_on_site_root
    sleep 2
    context_menu_on_site_root.click
    sleep 2
    new_page_option_in_context_menu.click
    sleep 2
    name_of_page_field.click
    sleep 2
    name_of_page_field.clear
    sleep 2
    name_of_page_field.send_keys(page_title)
    sleep 2
    pages_list_element.h3_element(:xpath => "//h3[contains(text(),'#{page_type}')]").click
    sleep 2
  end

  def set_created_list_page_as_list_root(list_page_title)
    begin
      sleep 2
      list_settings.click
    rescue
      retry
    end
    sleep 2
    open_mini_content_tree
    sleep 2
    set_page_in_mini_content_tree(list_page_title).click
    sleep 2
    ok_button_in_current_context_element.click
    sleep 2
  end

  def filter_by_page_type(page_type)
    page_type_filter_dropdown.click
    sleep 2
    page_type_filter_option(page_type).click
    sleep 2
  end

  def remove_tag_filter_setting
    begin
      sleep 2
      list_settings.click
    rescue
      retry
    end
    sleep 2
    remove_tag_filter.click
    sleep 2
  end

  def set_list_layout(list_layout)
    list_item_layout_dropdown.click
    sleep 2
    list_item_layout_option(list_layout).click
    sleep 2
  end

  def list_item_specific_properties(intro, publish_date, page_size)
    intro_checkbox_list_setting.click if intro
    sleep 2
    publish_date_checkbox_list_setting.click if publish_date
    sleep 2
    page_size_setting.clear
    sleep 2
    page_size_setting.send_keys page_size
  end

  def create_new_block(block_area, block_title, block_type)
    sleep 5
    case block_area
      when "Advanced Content"
        create_in_advanced_content_area.when_present.click
      when "Media Content"
        create_in_media_content_area.when_present.click
      when "Top Sidebar Content"
        create_in_top_sidebar_content.when_present.click
      else
        create_in_bottom_sidebar_content.when_present.click
    end
    name_of_page_field.when_present.click
    sleep 2
    name_of_page_field.clear
    sleep 2
    name_of_page_field.send_keys(block_title)
    sleep 3
    pages_list_element.h3_element(:xpath => "//h3[contains(text(),'#{block_type}')]").fire_event 'onclick'
    sleep 2
  end

  def navigate_on_view_mode_of_current_page_as_regular_user
    sleep 2
    options_menu_element.click if !page_href_element.visible?
    sleep 1
    clear_the_cookies
    page_href_element.click
    sleep 3
  end

  def create_content_pages_under_list_page(num_of_pages, page_types, list_page_title, content_pages_title, tag_set_approach, list_image)
    num_of_pages.times do |i|
      sleep 3
      navigate_on_specific_page(list_page_title).click
      sleep 3
      context_menu_on_specific_page(list_page_title).click
      sleep 1
      new_page_option_in_context_menu.click
      sleep 2
      name_of_page_field.click
      sleep 2
      name_of_page_field.clear
      sleep 2
      name_of_page_field.send_keys(content_pages_title+" #{i}")
      sleep 2
      pages_list_element.h3_element(:xpath => "//h3[contains(text(),'#{page_types}')]").click
      sleep 2
      default_text_field("Fill", content_pages_title, FigNewton.article_page_intro, FigNewton.article_page_body)
      sleep 2
      publish_current_page
      sleep 2
      set_tag(tag_set_approach)
      sleep 2
      set_content_list_image if list_image
      sleep 2
      publish_the_changes
    end
  end

  def delete_block_from_content_area(block_area)
    sleep 5
    case block_area
      when "Advanced Content"
        advance_block_menu.click
        sleep 2
        @browser.spans(:xpath => "//span[contains(concat(' ',normalize-space(@title),' '),'Display menu')]")[3].click
      when "Top Sidebar Content"
        top_sidebar_content_menu.click
        sleep 2
        @browser.spans(:xpath => "//span[contains(concat(' ',normalize-space(@title),' '),'Display menu')]")[2].click
      when "Media Content"
        media_block_menu.click
        sleep 2
        @browser.spans(:xpath => "//span[contains(concat(' ',normalize-space(@title),' '),'Display menu')]")[1].click
      else
        bottom_sidebar_content_menu.click
        sleep 2
        @browser.spans(:xpath => "//span[contains(concat(' ',normalize-space(@title),' '),'Display menu')]")[1].click
    end
    sleep 2
    if block_area == "Media Content"
      remove_option_for_media_block_item
    else
      remove_option_for_block_item
    end
  end

  def default_text_field(action, heading_text, intro_text, body_text)
    case action
      when "Fill"
        set_the_value_to_the_heading(heading_text)
        sleep 2
        set_the_value_to_the_intro(intro_text)
        sleep 2
        set_the_value_to_the_body(body_text)
        sleep 2
      when "Clear"
        clear_the_value_from_the_heading
        sleep 2
        clear_the_value_from_the_intro
        sleep 2
        clear_the_value_from_the_body
        sleep 2
      else
        sleep 5
        update_the_value_in_heading(heading_text)
        sleep 2
        update_the_value_in_intro(intro_text)
        sleep 2
        update_the_value_in_body(body_text)
        sleep 2
    end
  end

  def delete_created_content_page(page_title)
    sleep 2
    div_element(:xpath => "//div[contains(@title,'#{page_title}')]").span_element(:class => 'epi-extraIconsContainer').span_element.click
    sleep 2
    menu_items_elements[3].click
    sleep 3
    @browser.send_keys :enter
  end

  def set_tag(approach)
    open_tags_box
    sleep 1
    case approach
      when "Existing"
        define_existing_tag(FigNewton.tag_title_existing_cutted)
        sleep 2
      else
        define_new_tag(FigNewton.tag_title_new)
        sleep 2
    end
  end

  def set_tag_filter(tag)
    begin
      sleep 2
      list_settings.click
    rescue
      retry
    end
    sleep 2
    tags_filter.send_keys tag
    sleep 2
    @browser.send_keys :enter
  end


  def set_share_items(icons_title)
    open_share_tab_settings
    sleep 2
    icons_title.each do |icon|
      share_items[icon].click
    end
  end

  def set_content_list_image
    in_line_editing_mode_element.when_present.click
    sleep 2
    expand_and_choose_image_in_media_tree
    sleep 3
    navigate_on_on_page_editing_mode
    sleep 2
  end

  def fill_and_publish_richtext_block(body_text)
    sleep 2
    block_rich_text.send_keys body_text
    sleep 3
    create_block
    sleep 2
  end

  def fill_and_publish_image_with_caption_block(caption, description)
    choose_image_in_media_tree
    sleep 2
    caption_field_within_image_block.send_keys caption
    sleep 2
    description_field_within_image_block.send_keys description
    sleep 3
    create_block
    sleep 2
  end

  def tag_panel_contains_tag_with_title
    sleep 2
    get_tag_title_from_tag_bar_on_page_editing_mode
  end

  def copy_current_aritcle_page(page_title)
    sleep 2
    context_menu_on_specific_page(page_title).click
    sleep 2
    copy_page_option_in_context_menu.click
  end

  def paste_article_page_under_list_page(page_title)
    context_menu_on_specific_page(page_title).click
    sleep 2
    paste_page_option_in_context_menu.click
    sleep 6
  end

  def set_the_value_in_global_variable(variable, value)
    file = YAML::load_file('test_data/global_variables.yml') #Load
    file["#{variable}"] = value #Modify
    File.open('test_data/global_variables.yml', 'w') { |f| f.write file.to_yaml } #Store
  end

  def get_the_value_in_global_variable(variable)
    file = YAML::load_file('test_data/global_variables.yml') #Load
    file["#{variable}"]
  end

  def url_of_page_in_edit_mode
    @browser.url
  end

  def url_of_page_in_view_mode
    page_href_element.href
  end

  def clear_the_cookies
    @browser.cookies.clear
  end

  def navigate_on_site_root
    @browser.goto 'dev-cbaweb01001.osldev.creuna.int/EPiServer/Cms/#context=epi.cms.contentdata:///820'
  end

  def navigate_on_page_modes(url)
    sleep 2
    @browser.goto url
    sleep 2
  end

  def clear_content_list_image
    in_line_editing_mode_element.when_present.click
    clear_button.when_present.click
    navigate_on_on_page_editing_mode
  end

  def navigate_on_edit_mode_of_page_from_view_mode(page_url)
    navigate_on_page_modes(page_url)
    login_to_episerver()
    sleep 3
  end

  def clear_the_browser_cookies
    clear_the_cookies
  end
end